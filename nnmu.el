;;; nnmu.el --- mu backend for Gnus -*- lexical-binding: t -*-

;; Copyright (C) 2020 Jai Flack

;; Author: Jai Flack <jflack@disroot.org>
;; Created: 2020-09-14
;; Keywords: Gnus mu

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary

;; This package provides an `nnmu' backend for Gnus as an alternative
;; to `nnmaildir' or `nnnotmuch' which is also not a part of GNU Emacs.

;; The documentation for most functions and variables has been
;; intentionally left blank, instead refer to the Gnus manual.

;;; Code:

(require 'nnheader)
(require 'nnmaildir)
(require 'nnoo)


(defcustom nnmu-program
  (or (executable-find "mu")
      (cond
       ((file-exists-p "/usr/sbin/mu") "/usr/sbin/mu")
       ((file-exists-p "/usr/lib/mu") "/usr/lib/mu")
       ((file-exists-p "/usr/ucblib/mu") "/usr/ucblib/mu")
       (t "mu")))
  "Path to the mu program."
  :type 'file)

(defcustom nnmu-muhome ""
  "Path to the mu database"
  :type 'directory)


(nnoo-declare nnmu nnmaildir)
(nnoo-define-basics nnmu)


(defvar nnmu--current-group nil)
(defvar nnmu--current-server nil)

(deffoo nnmu-retrieve-headers (articles &optional group server fetch-old)
  (or group (setq group nnmu--current-group))
  )


(gnus-declare-backend "nnmu" 'post-mail 'address)

(provide 'nnmu)
